/*
  Sexp Reader, a s-expression reader written in C.

  Copyright (C) 2022 Miguel Ángel Martínez Quevedo

  This file is part of Sexp Reader.

  Sexp Reader is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Sexp Reader is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Sexp Reader. If not, see <https://www.gnu.org/licenses/>. 
*/

#include "sexp_reader.h"

#include "sexp.h"

#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define MAX_BUFFER_SIZE 1024

static void reader_init_with_file (const char* file_name);
static void reader_init_with_stringz (const char* stringz);
static struct sexp* read_all ();

struct sexp*
sexp_reader_read_file (char *file_name)
{
  reader_init_with_file (file_name);
  return read_all ();
}

struct sexp*
sexp_reader_read_string (char *string)
{
  reader_init_with_stringz (string);
  return read_all ();
}

static char buffer[MAX_BUFFER_SIZE + 1];
static size_t buffer_size;
static char *ptr;

static struct sexp* read_next ();
static struct sexp* read_atom ();
static struct sexp* read_list ();
static void read_space ();

static struct sexp*
report_input_error ()
{
  // TODO
  return NULL;
}

void
reader_init_with_file (const char* file_name)
{
  for (int i = 0; i < MAX_BUFFER_SIZE; i++)
    buffer[i] = 0;
  FILE *file = fopen (file_name, "r");
  fseek (file, 0, SEEK_END);
  long file_size = ftell (file);
  fseek (file, 0, SEEK_SET);
  if (file_size > MAX_BUFFER_SIZE) file_size = MAX_BUFFER_SIZE;
  long read_bytes = fread (buffer, 1, MAX_BUFFER_SIZE, file);
  assert (read_bytes == file_size);
  buffer_size = file_size;
  buffer[buffer_size] = 0;
  ptr = buffer;
}

void
reader_init_with_stringz (const char* stringz)
{
  for (int i = 0; i < MAX_BUFFER_SIZE; i++)
    buffer[i] = 0;
  long stringz_length = strlen (stringz);
  if (stringz_length > MAX_BUFFER_SIZE) stringz_length = MAX_BUFFER_SIZE;
  memcpy (buffer, stringz, stringz_length);
  buffer_size = stringz_length;
  buffer[buffer_size] = 0;
  ptr = buffer;
}

static struct sexp*
read_all ()
{
  struct sexp *list = sexp_new_list ();
  while (*ptr)
    {
      struct sexp *element = read_next ();
      if (element)
	sexp_list_push_back (list, element);
      else break;
    }  
  return list;
}

static struct sexp*
read_next ()
{
  read_space ();
  switch (*ptr)
    {
    case 0:
      return NULL;
    case '(':
      return read_list ();
    case ')':
      return report_input_error ();
    default:
      return read_atom ();
    }
}

static struct sexp*
read_atom ()
{
  assert (*ptr && *ptr != '(' && *ptr != ')');
  struct sexp* sexp = sexp_new_atom ();
  if (*ptr == '"') // TODO: handle escape sequences.
    {
      sexp_atom_insert_char (sexp, '"');
      ptr++;
      for (;;)
	{
	  switch (*ptr)
	    {
	    case '"':
	      sexp_atom_insert_char (sexp, '"');
	      ptr++;
	      return sexp;
	    case 0:
	      sexp_destroy (sexp);
	      return report_input_error ();
	    case '\\':
	      ptr++;
	      if (*ptr == 0)
		{
		  sexp_destroy (sexp);
		  return report_input_error ();
		}
	    default:
	      sexp_atom_insert_char (sexp, *ptr);
	      ptr++;
	    }
	}
    }
  else
    {
      assert (!isspace (*ptr));
      for (; *ptr &&
	     !isspace(*ptr) &&
	     *ptr != '(' && *ptr != ')';
	   ptr++)
	{
	  sexp_atom_insert_char (sexp, *ptr);
	}
    }
  return sexp;
}

static struct sexp*
read_list ()
{
  assert (*ptr == '(');
  struct sexp *list = sexp_new_list ();
  ptr++;
  read_space ();  
  for (; *ptr && *ptr != ')'; read_space ())
    {
      struct sexp *element = read_next ();
      sexp_list_push_back (list, element);
    }
  if (*ptr)
    {
      assert (*ptr == ')');
      ptr++;
      return list;
    }
  return report_input_error ();
}

static void
read_space ()
{
  while (*ptr && isspace (*ptr))
    {
      ptr++;
    }
}
