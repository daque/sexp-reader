/*
  Sexp Reader, a s-expression reader written in C.

  Copyright (C) 2022 Miguel Ángel Martínez Quevedo

  This file is part of Sexp Reader.

  Sexp Reader is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Sexp Reader is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Sexp Reader. If not, see <https://www.gnu.org/licenses/>. 
*/

#pragma once

#include "sexp.h"

struct sexp* sexp_reader_read_file (char *file_name);
struct sexp* sexp_reader_read_string (char *string);
