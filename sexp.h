/*
  Sexp Reader, a s-expression reader written in C.

  Copyright (C) 2022 Miguel Ángel Martínez Quevedo

  This file is part of Sexp Reader.

  Sexp Reader is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Sexp Reader is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Sexp Reader. If not, see <https://www.gnu.org/licenses/>. 
*/

#pragma once

#include <stdlib.h>

enum sexp_type
  {
    SEXP_TYPE_LIST,
    SEXP_TYPE_ATOM
  };

struct sexp_atom
{
  char *chars;
  size_t capacity;
  size_t length;
};

struct sexp_list
{
  struct sexp *front, *back;
};

struct sexp
{
  enum sexp_type type;
  struct sexp *parent, *next, *previous;
  union
  {
    struct sexp_atom atom;
    struct sexp_list list;
  };
};

struct sexp* sexp_new_atom ();
struct sexp* sexp_new_atom_from_stringz (const char *stringz);
struct sexp* sexp_new_list ();
void sexp_destroy (struct sexp* value);
void sexp_atom_insert_char (struct sexp* atom, char ch);
void sexp_list_push_back (struct sexp* list, struct sexp* new_back);
void sexp_list_push_front (struct sexp* list, struct sexp* new_front);
void sexp_print (struct sexp* value);
