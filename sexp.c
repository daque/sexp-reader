/*
  Sexp Reader, a s-expression reader written in C.

  Copyright (C) 2022 Miguel Ángel Martínez Quevedo

  This file is part of Sexp Reader.

  Sexp Reader is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Sexp Reader is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Sexp Reader. If not, see <https://www.gnu.org/licenses/>. 
*/

#include "sexp.h"

#include <stdlib.h>
#include <assert.h>
#include <string.h>

struct sexp*
sexp_new_atom ()
{
  struct sexp* atom = malloc (sizeof (struct sexp));
  atom->type = SEXP_TYPE_ATOM;
  atom->parent = NULL;
  atom->next = NULL;
  atom->previous = NULL;
  atom->atom.chars = malloc (1);
  atom->atom.capacity = 1;
  atom->atom.length = 0;
  return atom;
}

struct sexp*
sexp_new_atom_from_stringz (const char *stringz)
{
  int stringz_length = strlen (stringz);
  struct sexp* atom = malloc (sizeof (struct sexp));
  atom->type = SEXP_TYPE_ATOM;
  atom->parent = NULL;
  atom->next = NULL;
  atom->previous = NULL;
  atom->atom.chars = malloc (stringz_length + 1);
  atom->atom.capacity = stringz_length + 1;
  atom->atom.length = stringz_length;
  memcpy (atom->atom.chars, stringz, stringz_length);
  return atom; 
}

struct sexp*
sexp_new_list ()
{
  struct sexp* list = malloc (sizeof (struct sexp));
  list->type = SEXP_TYPE_LIST;
  list->parent = NULL;
  list->next = NULL;
  list->previous = NULL;
  list->list.front = NULL;
  list->list.back = NULL;
  return list;
}

void
sexp_destroy (struct sexp* value)
{
  if (value->type == SEXP_TYPE_ATOM)
    {
      free (value->atom.chars);
      free (value);
      return;
    }
  for (struct sexp *it = value->list.front;
       it;
       it = it->next)
    sexp_destroy (it);
  free (value);
  return;
}

void
sexp_atom_insert_char (struct sexp *atom, char ch)
{
  if (atom->atom.length + 1 > atom->atom.capacity)
    {
      atom->atom.capacity *= 2;
      atom->atom.chars = realloc (atom->atom.chars,
				  atom->atom.capacity);
    }
  assert (atom->atom.length + 1 <= atom->atom.capacity);
  atom->atom.length++;
  atom->atom.chars[atom->atom.length - 1] = ch;
}

void
sexp_list_push_back (struct sexp* list, struct sexp* element)
{
  assert (element);
  element->parent = list;
  if (list->list.front == NULL)
    {
      list->list.front = element;
      list->list.back = element;
      return;
    }
  list->list.back->next = element;
  element->previous = list->list.back;
  list->list.back = element;
}

void
sexp_list_push_front (struct sexp* list, struct sexp* element)
{
  assert (element);
  element->parent = list;
  if (list->list.front == NULL)
    {
      list->list.front = element;
      list->list.back = element;
      return;
    }
  list->list.front->previous = element;
  element->next = list->list.front;
  list->list.front = element;
}

#include <stdio.h>

static void
print_indentation (int depth)
{
  for (int i = 0; i < depth; i++)
    {
      putchar (' ');
      putchar (' ');
    }
}

static void
print_auxiliar (struct sexp* value, int indented, int depth)
{
  if (!indented) print_indentation (depth);
  if (value->type == SEXP_TYPE_ATOM)
    {
      for (int i = 0; i < value->atom.length; i++)
	{
	  putchar (value->atom.chars[i]);
	}
    }
  else
    {
      putchar ('(');
      for (struct sexp *it = value->list.front;
	   it;
	   it = it->next)
	{
	  if (it == value->list.front)
	    {
	      print_auxiliar (it, 1, depth + 1);
	    }
	  else
	    {
	      putchar ('\n');
	      print_auxiliar (it, 0, depth + 1);
	    }
	}
      putchar (')');
    }
}

void
sexp_print (struct sexp* value)
{
  print_auxiliar (value, 0, 0);
}
